# README #
This repository hosts the source code for an application that can be called by SourceTree, will take the $PWD, $LOCAL and $REMOTE parameters, fix the slashes to work with windows, change the $REMOTE relative path to an absolute path if needed and then launch LVCompare.

* $PWD - The root path of your repository.
* $LOCAL - The path to the "Mine" or current version of the vi.
* $REMOTE - The path to the "Theirs" version of the vi. This is absolute if not currently checked out and relative if checked out, so we need to prepend $PWD if it is relative.
* Currently this application calls the 32 bit LVCompare and makes "External Diff" always use LVCompare.

### Using the Application ###

* Download and store SourceTreeToLVCompare.exe somewhere on your local hard drive.
* In SourceTree, in Tools->Options->Diff
* External Diff Tool: Custom
* Diff Command: <Path to the downloaded exe>
* Arguments: $PWD $LOCAL $REMOTE

* Then when you right click on a vi and select "External Diff" it will call SourceTreeToLVCompare.exe which just fixes up the paths and calls C:\Program Files (x86)\National Instruments\Shared\LabVIEW Compare\LVCompare.exe

### Who do I talk to? ###

* Carl Wecker carl@endigit.com 801.879.0630